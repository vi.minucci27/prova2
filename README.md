# Comandi Shell
Lo scopo di questo README è quello di creare una piccola enciclopedia dei comandi più utili della shell Ubuntu.
## Indice   
1. [`grep` Ricerca espressioni all'interno di un file](#id1)
2. [`cat` (zcat)](#id2)
3. [`awk`](#id3)
4. [`sed` ](#id4)
5. [`cut` cut](#id5)
6. [`paste` paste](#id6)
7. [`wc` wc](#id7)
8. [`touch` touch](#id8)
9. [`zip` zip](#id9)
10. [`find` find](#id10)
11. [`more` more](#id11)
12. [`|`  pipe ](#id12)
13. [`vi`Editor di Testo](#id13)
14. [`export`](#id143)

## grep (zgrep)<a name="id1"></a>
Il comando grep viene utilizzato per cercare espressioni da un dato file. 
In particolare se il file è compresso è possibile utilizzare zgrep

**Nota:Tutte le opzioni che si applicano al comando grep si applicano anche al comando zgrep.**

**Comando**
```sh
grep [grep options] espressione nome_file
```
**Esempio**
```sh
zgrep -c "linux" GFG.txt.gz
```
Nell' esempio precedente riceveremo in output il numero risultati per la ricerca della parola "linux" nel file "GFG.txt.gz".

**Opzioni**
| Comando | Descrizione |
| ------ | ------ |
|-c  |mostra il numero di righe in cui trova l'espressione ricercata |
| -i |ignora la case sensitivity|
| -n | mostra il numero della riga in cui viene trovata l'espressione ricercata|
| -v |mostra le righe in cui non viene trovata l'espressione|
|-e| serve per effettuare ricerche multiple|
|-o|  ??|
|-l|  mostra il nome dei file in cui l'espressione è presente|
|-w|  ??|
|-h|??|


## cat (zcat)<a name="id2"></a>
comando cat
## awk<a name="id3"></a>
comando awk
## sed<a name="id4"></a>
comando sed
## cut<a name="id5"></a>
comando cut
## paste<a name="id6"></a>
comando paste
## wc<a name="id7"></a>
comando wc
## touch<a name="id8"></a>
comando touch
## zip<a name="id9"></a>
comando zip
## find<a name="id10"></a>
comando find
## more<a name="id11"></a>
Comando more
## | pipe<a name="id12"></a>
Comando pipe
## vi Editor di Testo<a name="id13"></a>

**Comando**
```sh
vi nome_file
```
**Spostarsi nel testo**
| Comando | Descrizione |
| ------ | ------ |
| h |sposta il cursore uno spazio a destra |
| j | sposta il cursore una linea in basso|
| k | sposta il cursore una linea in alto|
| l | sposta il cursore una linea a destra|
|w | sposta all'inizio della parola seguente|
|e|  sposta alla fine della parola seguente|
|E|  sposta alla fine della parola seguente prima di uno spazio|

Nota: In alcune versioni è possibile compiere le stesse azioni con i tasti freccia.

**Modificare Testo**
| Comando | Descrizione |
| ------ | ------ |
| h |sposta il cursore uno spazio a destra |
| j | sposta il cursore una linea in basso|
| k | sposta il cursore una linea in alto|
| l | sposta il cursore una linea a destra|
|w   	      |sposta all'inizio della parola seguente|
|e          |sposta alla fine della parola seguente|
|E          |sposta alla fine della parola seguente prima di uno spazio|
|b          |sposta all'inizio della parola precedente|
|0          |sposta all'inizio della linea|
|^          |sposta alla prima parola della linea corrente|
|$          |sposta alla fine della linea|
|<CR>       |sposta all'inizio della linea seguente|
|-          |sposta all'inizio della linea precedente|
|G          |sposta alla fine del file|
|1G         |sposta all'inizio del file|
|nG        | sposta alla linea numero n|
|<Cntl> G  | mostra il numero di linea corrente|
|%          |alla parentesi corrispondente|
|H         | alla prima linea della schermata|
|M         |alla linea centrale della schermata|
|L         | all'ultima linea della schermata|
|n|        |sposta il cursore alla colonna n|


